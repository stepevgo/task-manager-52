package ru.t1.stepanishchev.tm.command.data;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.stepanishchev.tm.api.endpoint.IDomainEndpoint;
import ru.t1.stepanishchev.tm.command.AbstractCommand;

@NoArgsConstructor
public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    public IDomainEndpoint getDomainEndpoint() {
        return getServiceLocator().getDomainEndpoint();
    }

}