package ru.t1.stepanishchev.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.stepanishchev.tm.dto.model.ProjectDTO;
import ru.t1.stepanishchev.tm.enumerated.ProjectSort;
import ru.t1.stepanishchev.tm.enumerated.Status;

import java.util.Collection;
import java.util.List;

public interface IProjectDTOService extends IUserOwnedDTOService<ProjectDTO> {

    @NotNull
    String getSortType(@Nullable ProjectSort sort);

    @NotNull
    ProjectDTO create(
            @Nullable String userId,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    ProjectDTO changeProjectStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    void clear(@Nullable String userId);

    void clear();

    @NotNull
    List<ProjectDTO> findAll();

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId);

    @NotNull
    List<ProjectDTO> findAll(@Nullable String userId, @Nullable ProjectSort sort);

    @Nullable
    ProjectDTO findOneById(
            @Nullable String userId,
            @Nullable String id
    );

    @NotNull
    ProjectDTO removeOneById(
            @Nullable String userId,
            @Nullable String id
    );

    void set(@NotNull Collection<ProjectDTO> projects);

    @NotNull
    ProjectDTO updateOneById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

}